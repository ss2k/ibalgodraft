from ibapi.wrapper import EWrapper
from ibapi.client import EClient
from ibapi.order import Order
from ibapi.contract import Contract

from ibapi.ticktype import TickTypeEnum

import time, threading
import schedule
from time import sleep
import queue
import datetime

import requests

TIME_OUT = object()

TOTAL_BALANCE = 0
OWNED_QUANTITY = 0 
OWNED_TICKER = ""
STOCK_PRICE = 0
NUM_STOCKS_TO_BUY = 0

class IBWrapper(EWrapper):
  def __init__(self):
    self = self

    # self.reqAccountUpdates(True, self)

class IBClient(EClient):
  def __init__(self, wrapper):
    EClient.__init__(self, wrapper)

class IBApp(IBWrapper, IBClient):
  def __init__(self):
    IBWrapper.__init__(self)
    IBClient.__init__(self, wrapper = self)
    self.nextValidOrderId = None

    # self.reqAccountUpdates(True, self.account)

  # def updateAccountValue(self, key: str, val: str, currency: str,
  #                       accountName: str):
  #   super().updateAccountValue(key, val, currency, accountName)
  #   print("UpdateAccountValue. Key:", key, "Value:", val,
  #         "Currency:", currency, "AccountName:", accountName)

  def nextValidId(self, orderId: int):
    super().nextValidId(orderId)
    print(orderId)
    self.nextValidOrderId = orderId
    
    self.start()
  
  def nextOrderId(self):
    oid = self.nextValidOrderId
    self.nextValidOrderId += 1
    return oid

  def updatePortfolio(self, contract: Contract, position: float, marketPrice: float,
    marketValue: float, averageCost: float, unrealizedPNL: float, realizedPNL: float,
    accountName: str):
      if position > 0:
        global OWNED_TICKER
        global OWNED_QUANTITY
        OWNED_TICKER = contract.symbol
        OWNED_QUANTITY = int(position) # returns float otherwise
      # print("UpdatePortfolio.", "Symbol:", contract.symbol, "SecType:",
      #   contract.secType, "Exchange:", contract.exchange,
      #   "Position:", position, "MarketPrice:", marketPrice, "MarketValue:",
      #   marketValue, "AverageCost:", averageCost,
      #   "UnrealizedPNL:", unrealizedPNL, "RealizedPNL:", realizedPNL,
      #   "AccountName:", accountName)
  # def updatePortfolio(self, contract: Contract, position: float,
  #                     marketPrice: float, marketValue: float,
  #                     averageCost: float, unrealizedPNL: float,
  #                     realizedPNL: float, accountName: str):
  #     super().updatePortfolio(contract, position, marketPrice, marketValue,
  #                             averageCost, unrealizedPNL, realizedPNL, accountName)
  #     print("UpdatePortfolio.", contract.symbol, "", contract.secType, "@",
  #           contract.exchange, "Position:", position, "MarketPrice:", marketPrice,
  #           "MarketValue:", marketValue, "AverageCost:", averageCost,
  #           "UnrealizedPNL:", unrealizedPNL, "RealizedPNL:", realizedPNL,
  #           "AccountName:", accountName)
  def updateAccountValue(self, key: str, val: str, currency: str, accountName: str):
    if key == 'TotalCashBalance' and currency == 'USD':
      # print("UpdateAccountValue. Key:", key, "Value:", val, "Currency:", currency,
      #   "AccountName:", accountName)
      global TOTAL_BALANCE
      TOTAL_BALANCE = float(val) - 10000.0
      print(TOTAL_BALANCE)
  # def updateAccountTime(self, timeStamp: str):
  #     super().updateAccountTime(timeStamp)
  #     print("UpdateAccountTime. Time:", timeStamp)

  # def accountDownloadEnd(self, accountName: str):
  #     super().accountDownloadEnd(accountName)
  #     print("Account download finished:", accountName)

  def position(self, account: str, contract: Contract, position: float, avgCost: float):
    super().position(account, contract, position, avgCost)
    # print("Position.", "Account:", account, "Symbol:", contract.symbol, "SecType:",
    #   contract.secType, "Currency:", contract.currency,
    #   "Position:", position, "Avg cost:", avgCost)

  def error(self, reqId, errorCode, errorString):
    print("Error: ", reqId, " ", errorCode, " ", errorString)
  
  def tickPrice(self, reqId, tickType, price, attrib):
    if TickTypeEnum.to_str(tickType) == "DELAYED_LAST":
      global STOCK_PRICE
      STOCK_PRICE = float(price)
    # print("Tick Price. Ticker Id:", reqId, "tickType:",
    #   TickTypeEnum.to_str(tickType), "Price:", price, end=' ')
  
  # def tickSize(self, reqId, tickType, size):
  #   print("tick size")
    # print("Tick Size. Ticker Id:", reqId, "tickType:",
    #   TickTypeEnum.to_str(tickType), "Size:", size)

  def start(self):
    print('started')
    # self.reqAccountUpdates(True, "")
    # def get_
    # contract = Contract()
    # contract.symbol = "AAPL"
    # contract.secType = "STK"
    # contract.currency = "USD"
    # contract.exchange = "SMART"

    # self.reqMarketDataType(4) # switch to delayed-frozen data if live is not available
    # self.reqMktData(1, contract, "", False, False, [])
    # print(self.nextValidOrderId)
    # print(self.nextOrderId())
    # self.read_manuscript()
    # self.reqAccountUpdates(True, "")
    # self.reqPositions(); #returns position()
    self.read_manuscript()
    # schedule.every(2).seconds.do(self.read_manuscript)
    # while True:
    #   schedule.run_pending()
    # self.start_order_process()
  
  def keyboardInterrupt(self):
    self.stop()

  def stop(self):
    print('stopped')
    self.reqAccountUpdates(False, "")
    self.done = True
    self.disconnect()
  
  def calculate_num_stocks(self, ticker):
    contract = Contract()
    contract.symbol = ticker
    contract.secType = "STK"
    contract.currency = "USD"
    contract.exchange = "SMART"

    self.reqMarketDataType(4)
    self.reqMktData(1, contract, "", False, False, [])
    print('calculate_num_stocks')
    print(TOTAL_BALANCE)
    print(STOCK_PRICE)
    if TOTAL_BALANCE > 0 and STOCK_PRICE > 0:
      global NUM_STOCKS_TO_BUY
      NUM_STOCKS_TO_BUY = int(TOTAL_BALANCE / STOCK_PRICE)

  def num_stocks_owned(self, ticker):
    print('nso')
    # contract info
    contract = Contract()
    contract.symbol = ticker
    contract.secType = "STK"
    contract.currency = "USD"
    contract.exchange = "SMART"

    self.reqAccountUpdates(True, "")
    # self.reqMarketDataType(4)
    # self.reqMktData(1, contract, "", False, False, [])

  def read_manuscript(self):
    self.reqAccountUpdates(True, "")
    # print('read manuscript')
    # print('total balance')
    # print(TOTAL_BALANCE)
    # print('owned ticker')
    # print(OWNED_TICKER)
    # print('owned quantity')
    # print(OWNED_QUANTITY)

    # threading.Timer(10, self.read_manuscript).start()
    ms = open("manuscript.txt", "r")
    ms_data = ms.readlines()
    if ms_data == []:
      last_bought = "None"
    elif len(ms_data) > 1:
      last_bought = ms_data[-1]
    else:
      last_bought = ms_data[0]
    ms.close()

    owned = open("owned.txt", "r")
    od_data = owned.readlines()
    if od_data == []:
      currently_owned = 'None'
    else:
      currently_owned = od_data[0]

    self.get_rsi(last_bought, currently_owned)

    threading.Timer(60, self.read_manuscript).start()

  def get_rsi(self,last_bought, currently_owned):
    print('get_rsi')
    url = 'http://localhost:9292/signal/drn/drv'
    r = requests.get(url)
    signal = r.json()['signal']
    self.run_flow(signal, last_bought, currently_owned)

  def run_flow(self, signal, last_bought, currently_owned):
    print(last_bought)
    print(currently_owned)
    print(signal)
    if signal <= -1:
      print('Signal < -1 ')
      if currently_owned == 'BEAR':
        print("No Action. Currently Own Bear")
      else:
        if currently_owned == "BULL":
          print("Selling BULL")
          self.num_stocks_owned("DRN")
          self.start_order_process("DRN", "SELL", OWNED_QUANTITY)
        else:
          if last_bought == 'BEAR':
            print('No Action. Manuscript shows BEAR')
          else:
            print("Buying BEAR")
            self.calculate_num_stocks("DRV")
            self.start_order_process("DRV", "BUY", NUM_STOCKS_TO_BUY)
    elif signal < 1:
      print('No Action: Signal Between -1 and +1')
    else:
      print('Signal > 1')
      if currently_owned == 'BULL':
        print("No Action. Currently Own Bull")
      else:
        if currently_owned == "BEAR":
          print("Selling Bear")
          self.num_stocks_owned("DRV")
          self.start_order_process("DRV", "SELL", OWNED_QUANTITY)
        else:
          if last_bought == 'BULL':
            print('No Action. Manuscript shows BULL')
          else:
            print('BUYING BULL')
            self.calculate_num_stocks("DRN")
            self.start_order_process("DRN", "BUY", NUM_STOCKS_TO_BUY)

  def start_order_process(self, ticker, action, quantity):
    print('order process')
    print(quantity)
    if quantity > 0:
      contract = Contract()
      contract.symbol = ticker
      contract.secType = "STK"
      contract.currency = "USD"
      contract.exchange = "SMART"

      order = Order()
      order.action = action
      order.orderType = "TRAIL"
      order.totalQuantity = quantity
      order.transmit = True

      if action == "BUY":
        order.trailingPercent = 3

      ticker_definition = { 
        'DRN': 'BULL',
        'DRV': 'BEAR'  
      }

      orderid = self.nextOrderId()

      self.placeOrder(
        orderid,  # orderId,
        contract,  # contract,
        order  # order
      )

      if action == "SELL":
        # clear conent of owned
        open('owned.txt', 'w').close()
      else:
        owned_file = open('owned.txt', 'w+')
        owned_file.write(ticker_definition[ticker])
        owned_file.close()

        manuscript_file = open('manuscript.txt', 'w+')
        manuscript_file.write(ticker_definition[ticker])
        manuscript_file.close()


def main():
  app = IBApp()
  app.connect('127.0.0.1', 7497, clientId=0)
  print('connected')

  app.run()

if __name__ == "__main__":
    main()